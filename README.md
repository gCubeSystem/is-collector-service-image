# IS Collector Service Docker image

This repo contains the Dockerfile to build the image of the gCore IS Collector service

## Build the image

```shell
$ docker build -t is-collector-service .
```

## Test the image

```shell
$ docker container run --name is-collector is-collector-service 
```
