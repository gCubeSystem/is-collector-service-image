FROM d4science/gcore-distribution

# Download and install the eXist database

RUN wget --no-check-certificate http://downloads.sourceforge.net/project/exist/Stable/1.2/eXist-setup-1.2.6-rev9165.jar && java -jar eXist-setup-1.2.6-rev9165.jar -p /home/gcube/exist
ENV EXIST_HOME=/home/gcube/exist
RUN cp $EXIST_HOME/exist.jar $GLOBUS_LOCATION/lib
RUN cp $EXIST_HOME/lib/core/quartz-1.6.0.jar $GLOBUS_LOCATION/lib
RUN cp $EXIST_HOME/lib/core/xmlrpc-* $GLOBUS_LOCATION/lib
RUN cp $EXIST_HOME/lib/core/xmldb.jar $GLOBUS_LOCATION/lib
RUN cp $EXIST_HOME/lib/core/jta.jar $GLOBUS_LOCATION/lib
RUN cp $EXIST_HOME/lib/core/commons-pool-1.4.jar $GLOBUS_LOCATION/lib

# RUN rm -fr $EXIST_HOME/webapp/WEB-INF/data $EXIST_HOME/webapp/WEB-INF/logs $EXIST_HOME/webapp/WEB-INF/attachments
# RUN ln -s /gcube-data/db/data $EXIST_HOME/webapp/WEB-INF/
# RUN ln -s /gcube-data/db/logs $EXIST_HOME/webapp/WEB-INF/
# RUN ln -s /gcube-data/db/attachments $EXIST_HOME/webapp/WEB-INF/

RUN wget --no-check-certificate https://nexus.d4science.org/nexus/content/repositories/gcube-staging-gcore/org/gcube/informationsystem/is-collector-service/3.0.2-4.15.0-126944/is-collector-service-3.0.2-4.15.0-126944.gar && mv is-collector-service-3.0.2-4.15.0-126944.gar org.gcube.informationsystem.collector.gar && gcore-deploy-service org.gcube.informationsystem.collector.gar

ENV GLOBUS_OPTIONS="-Xms2000M -Xmx2000M -Dexist.home=$EXIST_HOME"
